/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "common.h"

static void capFrameRate(long* then, float* remainder);

// char* global_dir = "C:\\Users\\di918039\\source\\repos\\cs1pr-portfolio\\project-visual-studio\\SpringProject";

int WinMain(){
	long then;
	float remainder;

	memset(&app, 0, sizeof(App));
	app.textureTail = &app.textureHead;

	initSDL();

	atexit(cleanup);

	initGame();

	SDL_SetRenderDrawColor(app.renderer, 128, 192, 255, 255);
	SDL_RenderClear(app.renderer);
	SDL_RenderPresent(app.renderer);

	SDL_Rect r;
	r.h = 50;
	r.w = 150;
	r.x = (SCREEN_WIDTH / 2) - (r.w / 2);
	r.y = (SCREEN_HEIGHT / 2) - (r.h / 2);

	SDL_SetRenderDrawColor(app.renderer, 128, 128, 128, 255);
	SDL_RenderFillRect(app.renderer, &r);
	drawText((SCREEN_WIDTH / 2), (SCREEN_HEIGHT / 2) - 12, 0, 0, 0, TEXT_CENTER, "START");
	SDL_RenderPresent(app.renderer);

	int x, y;
	SDL_Event event;
	while (1) {
		SDL_GetMouseState(&x, &y);
		SDL_PollEvent(&event);
		if ((r.x <= x && x <= r.x + r.w) && (r.y <= y && y <= r.y + r.h))
			if (event.type == SDL_MOUSEBUTTONDOWN)
				break;
	}

	initStage();
	//initP2S();

	then = SDL_GetTicks();

	remainder = 0;

	while (1)
	{
		prepareScene();

		doInput();

		app.delegate.logic();

		app.delegate.draw();

		presentScene();

		capFrameRate(&then, &remainder);
	}

	return 0;
}

static void capFrameRate(long* then, float* remainder)
{
	long wait, frameTime;

	wait = 16 + *remainder;

	*remainder -= (int)* remainder;

	frameTime = SDL_GetTicks() - *then;

	wait -= frameTime;

	if (wait < 1)
	{
		wait = 1;
	}

	SDL_Delay(wait);

	*remainder += 0.667;

	*then = SDL_GetTicks();
}
